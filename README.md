# M3 HW3 ALLEN SYD
Some trouble with mixins and the buttons. I keep getting errors in main.less that I don't understand. Can I meet with you after lecture this Thursday for help?

sources
https://getbootstrap.com/docs/4.4/layout/grid/
https://www.w3schools.com/css/css3_colors.asp
https://www.w3schools.com/howto/howto_css_hero_image.asp
https://lesscss.org/
https://www.w3schools.com/cssref/pr_text_text-transform.asp
https://www.w3schools.com/cssref/tryit.asp?filename=trycss_text-transform
https://www.w3schools.com/howto/howto_css_round_buttons.asp
https://www.w3schools.com/css/css3_buttons.asp
https://www.w3schools.com/howto/howto_css_image_overlay.asp
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_image_overlay_opacity
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_image_overlay_title
https://www.w3schools.com/howto/howto_css_hero_image.asp
https://www.w3schools.com/cssref/pr_font_weight.asp
https://www.w3schools.com/css/tryit.asp?filename=trycss_text
https://www.w3schools.com/css/css_text.asp
https://www.w3schools.com/css/tryit.asp?filename=trycss_buttons_basic
https://www.w3schools.com/css/css3_buttons.asp
https://fonts.google.com/specimen/Roboto#standard-styles